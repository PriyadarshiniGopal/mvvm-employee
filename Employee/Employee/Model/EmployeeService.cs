﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Employee.Model
{
    public class EmployeeService
    {
        private static ObservableCollection<Employee> emplist;
        public EmployeeService()
        {
            emplist=new ObservableCollection<Employee> {
            new Employee { Id = 1, Name = "jojo" }
            };
        }
        public ObservableCollection<Employee> GetAll()
        {
            return emplist;
        }
        public void AddEmployee(Employee emp)
        {
            emplist.Add(emp);
        }
        public void Update(Employee updateemp)
        {
            foreach(Employee emp in emplist)
            {
                if (emp.Id == updateemp.Id)
                {
                    emp.Name = updateemp.Name;
                    break;
                }
            }
        }

        public Employee Search(int id)
        {
            foreach (Employee emp in emplist)
            {
                if (emp.Id == id)
                {
                    return emp;
                }
            }
            return null;
        }
        public void Delete(int id)
        {
            foreach (Employee emp in emplist)
            {
                if (emp.Id == id)
                {
                    emplist.Remove(emp);
                    break;
                }
            }
        }
    }
}
