﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ComponentModel;
using Employee.Model;
using Employee.Commands;
using System.Windows;

namespace Employee.ViewModel
{
    public class EmployeeViewModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String PropertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
        private ObservableCollection<Model.Employee> emp;
        public ObservableCollection<Model.Employee> Emp
        {
            get { return emp; }
            set
            {
                emp = value;
                OnPropertyChanged("Emp");
            }
        }
        EmployeeService employeeServiceObject;
        public EmployeeViewModel()
        {
            employeeServiceObject= new EmployeeService();
            CurrentEmp = new Model.Employee();
            LoadData();
            search = new RelayCommand(findemp);
            //add = new RelayCommand(save);
            //delete = new RelayCommand(Delete);
            //update = new RelayCommand(Update);
        }
        private void LoadData()
        {
            Emp = employeeServiceObject.GetAll();
        }

        private Model.Employee currentemp;
        public Model.Employee CurrentEmp
        {
            get { return currentemp; }
            set
            {
                currentemp = value;
                OnPropertyChanged("CurrentEmp");
            }
        }
        private RelayCommand add;
        public RelayCommand Addcommand
        {
            get { return add; }
        }
        public void save()
        {
            Emp.Add(CurrentEmp);
            LoadData();
        }

        private RelayCommand search;
        public RelayCommand Searchcommand
        {
            get { return search; }
        }
        public void findemp()
        {
            var findemployee=employeeServiceObject.Search(CurrentEmp.Id);
            if (findemployee != null)
            {
                CurrentEmp.Name = findemployee.Name;
            }
        }

        private RelayCommand delete;
        public RelayCommand Deletecommand
        {
            get { return delete; }
        }
        public void Delete()
        {
            employeeServiceObject.Delete(CurrentEmp.Id);
            LoadData();
        }

        private RelayCommand update;
        public RelayCommand Updatecommand
        {
            get { return update; }
        }
        public void Update()
        {
            employeeServiceObject.Update(CurrentEmp);
            LoadData();
        }
    }
}
