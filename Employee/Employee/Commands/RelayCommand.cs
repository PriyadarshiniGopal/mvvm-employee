﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Employee.Commands
{
    public class RelayCommand :ICommand
    {
        public event EventHandler CanExecuteChanged;
        Action DoWork;
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public void Execute(object parameter)
        {
            DoWork();
        }

        public RelayCommand(Action work)
        {
            DoWork = work;
        }
    }
}
